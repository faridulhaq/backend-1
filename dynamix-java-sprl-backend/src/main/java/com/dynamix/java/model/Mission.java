package com.dynamix.java.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.dynamix.java.util.ReasonEnd;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Mission {

    private long id;
    private LocalDate startDate;
    private LocalDate endDate;
    private float rate;
    private String locationCity;
    private String description;
    private String evaluation;

    private ReasonEnd reasonEnd;
    
    private Contract contract;

    public Mission() {
    }

    public Mission(LocalDate startDate, LocalDate endDate, float rate, String locationCity, String description,
            String evaluation) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.rate = rate;
        this.locationCity = locationCity;
        this.description = description;
        this.evaluation = evaluation;
    }

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonFormat(pattern = "dd/MM/yyyy")
    @Column(nullable = false)
    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    @JsonFormat(pattern = "dd/MM/yyyy")
    @Column(nullable = false)
    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    @Column(nullable = false)
    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    
    public String getLocationCity() {
        return locationCity;
    }

    public void setLocationCity(String locationCity) {
        this.locationCity = locationCity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

    @Column(columnDefinition ="Enum('"+ ReasonEnd.Values.MISSION_END+"', '"+ ReasonEnd.Values.BUDGET_LACK+"', '"+ ReasonEnd.Values.CONSULTANT_PROBLEM+"','"+ ReasonEnd.Values.OLD_TECHNOLOGY+"')")
    public ReasonEnd getReasonEnd() {
        return reasonEnd;
    }

    public void setReasonEnd(ReasonEnd reasonEnd) {
        this.reasonEnd = reasonEnd;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contract_Id")
    public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	@Override
    public String toString() {
        return "Mission [description=" + description + ", endDate=" + endDate + ", evaluation=" + evaluation + ", id="
                + id + ", locationCity=" + locationCity + ", rate=" + rate + ", startDate=" + startDate + "]";
    }
    
}