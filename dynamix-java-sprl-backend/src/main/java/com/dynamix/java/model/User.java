package com.dynamix.java.model;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class User {

	private long userId;
	private String password;
	private String login;
	private Role role;
	private Employee employee;

	public User() {

	}

	public User(User user) {
		this.userId = user.getUserId();
		this.password = user.getPassword();
		this.login = user.getLogin();
		this.role = user.getRole();
	}

	public User(String password, String login, Role role, Employee employee) {
		this.password = password;
		this.login = login;
		this.role = role;
		this.employee = employee;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "userId")
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	@Column(name = "password", nullable = false)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "login", nullable = false)
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "roleId", nullable = false)
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@OneToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL)
	@JoinColumn(name = "employeeId",referencedColumnName = "employeeId" )
	@OnDelete(action = OnDeleteAction.CASCADE)
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", password=" + password + ", login=" + login + ", role=" + role
				+ ", employee=" + employee + "]";
	}

}
