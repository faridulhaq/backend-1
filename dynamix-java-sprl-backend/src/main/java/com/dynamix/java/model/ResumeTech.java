package com.dynamix.java.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "resume_tech")
public class ResumeTech {

    @EmbeddedId
    private ResumeTechId resumeTechId;

    @Column(name = "level", nullable = false)
    private String level;

    public ResumeTechId getResumeTechId() {
        return resumeTechId;
    }

    public void setResumeTechId(ResumeTechId resumeTechId) {
        this.resumeTechId = resumeTechId;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
