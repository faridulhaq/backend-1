package com.dynamix.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Company;
import com.dynamix.java.repository.CompanyRepository;

@Service
public class CompanyServiceImpl implements CompanyService {
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Override
	public List<Company> getAllCompanies() {
		return companyRepository.findAll();
	}

	@Override
	public Company getCompanyById(Long companyId) throws ResourceNotFoundException {
		return companyRepository.findById(companyId)
				.orElseThrow(() -> new ResourceNotFoundException("Company not found for this id :: " + companyId));
	}

	@Override
	public Company createCompany(Company company) {
		return companyRepository.save(company);
	}

	@Override
	public Company updateCompany(Long companyId, Company companyDetails) throws ResourceNotFoundException {
		Company company = companyRepository.findById(companyId)
				.orElseThrow(() -> new ResourceNotFoundException("Company not found for this id :: " + companyId));
		company.setName(companyDetails.getName());
		company.setVatNumber(companyDetails.getVatNumber());
		company.setComments(companyDetails.getComments());
		company.setCnyType(companyDetails.getCnyType());
		company.setAddress(companyDetails.getAddress());
		final Company updatedCompany = companyRepository.save(company);
		return updatedCompany;
	}

	@Override
	public void deleteCompany(Long companyId) throws ResourceNotFoundException {
		Company company = companyRepository.findById(companyId)
				.orElseThrow(() -> new ResourceNotFoundException("Company not found for this id :: " + companyId));
		companyRepository.delete(company);
	}

}
