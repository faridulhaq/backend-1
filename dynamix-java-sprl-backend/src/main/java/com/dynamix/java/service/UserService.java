package com.dynamix.java.service;

import java.util.List;
import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.User;


public interface UserService {
	
	List<User> getAllUsers();
	User getUserById(Long userId) throws ResourceNotFoundException;
	User createUser(User user);
	User updateUser(Long userId, User userDetails) throws ResourceNotFoundException;
	void deleteUser(Long userId) throws ResourceNotFoundException;

}
