package com.dynamix.java.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Mission;
import com.dynamix.java.repository.ContractRepository;
import com.dynamix.java.repository.MissionRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MissionService {

    @Autowired
    private MissionRepository missionRepository;
    
    @Autowired
    private ContractRepository contractRepository;

    public List<Mission> getAllMissions() {
		return missionRepository.findAll();
	}

	public Optional<Mission> getMissionById(Long missionId) {
		return missionRepository.findById(missionId);
	}
	
	public List<Mission> getMissionByContractId(Long contractId) {
		return missionRepository.findByContract_id(contractId);
	}

	public Mission createUpdateMission(Long contractId, @Valid Mission mission) throws ResourceNotFoundException {
        return contractRepository.findById(contractId).map(contract -> {
            mission.setContract(contract);
            return missionRepository.save(mission);
        }).orElseThrow(() -> new ResourceNotFoundException("ContractId " + contractId + " not found"));
	}
	
	public Map<String, Boolean> deleteMission(Long missionId) throws ResourceNotFoundException {
        Mission Mission = missionRepository.findById(missionId)
				.orElseThrow(() -> new ResourceNotFoundException("Mission not found for this id :: " + missionId));
                missionRepository.delete(Mission);
        
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}