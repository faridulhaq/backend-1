package com.dynamix.java.service;

import java.util.List;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Company;


public interface CompanyService {
	
	List<Company> getAllCompanies();
	Company getCompanyById(Long companyId) throws ResourceNotFoundException;
	Company createCompany(Company company);
	Company updateCompany(Long companyId, Company companyDetails) throws ResourceNotFoundException;
	void deleteCompany(Long companyId) throws ResourceNotFoundException;

}
