package com.dynamix.java.service;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Consultant;
import com.dynamix.java.model.Contact;

import java.util.List;

public interface ContactService {

    List<Contact> getAllContacts();

    Contact getContactById(Long id) throws ResourceNotFoundException;

    Contact saveContact(Contact contact);

    void deleteContact(Long id) throws ResourceNotFoundException;
}
