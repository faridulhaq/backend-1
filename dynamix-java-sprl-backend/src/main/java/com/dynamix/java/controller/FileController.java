package com.dynamix.java.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.File;
import com.dynamix.java.service.FileService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class FileController {
	@Autowired
	private FileService fileService;
	
	@GetMapping("/files")
	public List<File> getAllFiles() {
		return fileService.getAllFiles();
	}

	@GetMapping("/files/{resumeId}")
	public List<File> getFilesByResumeId(@PathVariable(value = "resumeId") Long resumeId) {
		return fileService.getFilesByResumeId(resumeId);
	}

	@PostMapping("/file")
	public File createFile(@Valid @RequestBody File file) {
		return fileService.createFile(file);
	}

	@PutMapping("/file/{id}")
	public ResponseEntity<File> updateFile(@PathVariable(value = "id") Long fileId,
			@Valid @RequestBody File fileDetails) throws ResourceNotFoundException {
		File updatedFile = fileService.updateFile(fileId, fileDetails);
		return ResponseEntity.ok(updatedFile);
	}

	@DeleteMapping("/file/{id}")
	public Map<String, Boolean> deleteFile(@PathVariable(value = "id") Long fileId)
			throws ResourceNotFoundException {
		fileService.deleteFile(fileId);;
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
