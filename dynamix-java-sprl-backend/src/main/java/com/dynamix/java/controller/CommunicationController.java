package com.dynamix.java.controller;

import com.dynamix.java.service.CommunicationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Communication;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class CommunicationController {
    
    @Autowired
    private CommunicationService communicationService;

    @GetMapping("/communications")
	public List<Communication> getAllCommunications() {
		return communicationService.getAllCommunications();
    }
    
    @GetMapping("/communications/{id}")
	public ResponseEntity<Communication> getCommunicationById(@PathVariable(value = "id") Long communicationId) throws ResourceNotFoundException { 
            Communication communication = communicationService.getCommunicationById(communicationId).orElseThrow(() -> 
										new ResourceNotFoundException("Communication not found for this id : " + communicationId));
										
		return ResponseEntity.ok().body(communication);
	}

    @GetMapping("/communications/employee/{id}")
	public List<Communication> getCommunicationByEmployeeId(@PathVariable(value = "id") Long employeeId) { 
		return communicationService.getCommunicationByEmployeeId(employeeId);
	}
	
	@GetMapping("/communications/contact/{id}")
	public List<Communication> getCommunicationByContactId(@PathVariable(value = "id") Long contactId) { 
		return communicationService.getCommunicationByContactId(contactId);
    }
    

    @PostMapping("/communications")
	public Communication createCommunication(@Valid @RequestBody Communication communication) {  
		System.out.println(communication); 
        return communicationService.createUpdateCommunication(communication);
    }


    @PutMapping("/communications/{id}")
	public ResponseEntity<Communication> updateCommunication(@PathVariable(value = "id") Long communicationId,
			@Valid @RequestBody Communication communicationDetails) throws ResourceNotFoundException {

		Communication communication = communicationService.getCommunicationById(communicationId)
				.orElseThrow(() -> new ResourceNotFoundException("Communication not found for this id : " + communicationId));

		communication.setCommunicationDate(communicationDetails.getCommunicationDate());
		communication.setContent(communicationDetails.getContent());
		final Communication updatedCommunication = communicationService.createUpdateCommunication(communication);
		return ResponseEntity.ok(updatedCommunication);
	}
    
    @DeleteMapping("/communications/{id}")
	public Map<String, Boolean> deleteCommunication(@PathVariable(value = "id") Long communicationId)
			throws ResourceNotFoundException {
		return communicationService.deleteCommunication(communicationId);
	}
}