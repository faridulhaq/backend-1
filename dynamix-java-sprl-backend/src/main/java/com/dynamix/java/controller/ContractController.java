package com.dynamix.java.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Contract;
import com.dynamix.java.service.ContractService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1/contracts")
public class ContractController {

    @Autowired
    private ContractService contractService;

    @GetMapping
	public List<Contract> getAllContracts() {
		return contractService.getAllContracts();
    }
    
	@GetMapping("/{id}")
	public ResponseEntity<Contract> getContractById(@PathVariable(value = "id") Long contractId)
			throws ResourceNotFoundException {
		Contract contract = contractService.getContractById(contractId)
				.orElseThrow(() -> new ResourceNotFoundException("Contract not found for this id : " + contractId));
		return ResponseEntity.ok().body(contract);
	}
	
	@GetMapping("/{contractId}/{consultantId}")
	public ResponseEntity<Boolean> getContractById(
			@RequestParam("endDate") @DateTimeFormat(pattern="dd/MM/yyyy") LocalDate endDate,
			@PathVariable(value = "contractId") Long contractId,
			@PathVariable(value = "consultantId") Long consultantId) throws ResourceNotFoundException {
		Contract contract = contractService.getContractByConsultantId(endDate, consultantId);
		if(contract != null){
			if(contractId != null && contract.getId() == contractId){
				return ResponseEntity.ok().body(false);
			} else {
				return ResponseEntity.ok().body(true);
			}
		} else {
			return ResponseEntity.ok().body(false);
		}
	}

    @PostMapping
	public ResponseEntity<Contract> createContract(@Valid @RequestBody Contract contract) {  
    	contract = contractService.createContract(contract);
        return ResponseEntity.ok().body(contract);
    }

	@PutMapping("/{id}")
	public ResponseEntity<Contract> updateContract(@PathVariable(value = "id") Long contractId,
			@Valid @RequestBody Contract contract) throws ResourceNotFoundException {
		final Contract updatedContract = contractService.updateContract(contract, contractId);
		return ResponseEntity.ok().body(updatedContract);
	}
    
    @DeleteMapping("/{id}")
	public Map<String, Boolean> deleteContract(@PathVariable(value = "id") Long contractId)
			throws ResourceNotFoundException {
		return contractService.deleteContract(contractId);
	}

}