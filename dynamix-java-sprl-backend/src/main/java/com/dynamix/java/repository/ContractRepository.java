package com.dynamix.java.repository;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dynamix.java.model.Contract;

@Repository
public interface ContractRepository extends JpaRepository<Contract, Long> {
	
	Contract findByStartDateLessThanEqualAndEndDateGreaterThanEqualAndConsultant_consultantId(LocalDate startDate,
			LocalDate endDate, Long consultantId);

}