package com.dynamix.java.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dynamix.java.model.File;

@Repository
public interface FileRepository extends JpaRepository<File, Long>{

	List<File> findByResume_resumeId(long resumeId);
}
