package com.dynamix.java.repository;

import java.util.List;
import java.util.Optional;

import com.dynamix.java.model.Communication;

import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommunicationRepository extends JpaRepository<Communication, Long>{

	List<Communication> findByEmployeeEmployeeId(@Param("employeeId") Long employeeId);

	List<Communication> findByContactContactId(@Param("contactId") Long contactId);

}