package com.example.security;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WelcomeController {
	

	@GetMapping(value="/welcome")
	public String welcome() {
		return "welcome Brother";
	}
}
