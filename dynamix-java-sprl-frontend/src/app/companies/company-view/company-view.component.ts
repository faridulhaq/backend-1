import { Component, OnInit } from '@angular/core';
import { Company, CompanyResolved } from 'src/app/companies/company';
import { ActivatedRoute, Router } from '@angular/router';
import { CompanyService } from '../company.service';

@Component({
  selector: 'app-company-view',
  templateUrl: './company-view.component.html',
  styleUrls: ['./company-view.component.css']
})
export class CompanyViewComponent implements OnInit {

  companyId: number;
  company: Company;
  resolvedData: CompanyResolved;
  error: string;
  
  constructor( private route:ActivatedRoute, private router: Router,
    private companyService: CompanyService ) {}

  ngOnInit(): void {

    this.route.data.subscribe( data => {
      if( data.resolvedData?.error ){
        this.error = data.error;
      } else {
        this.company = <Company>data.resolvedData;
      }
    }); 
  }

  list(){
    this.router.navigate(['companies']);
  }

  save(){
    
  }

  handleEditCompanyClick() {
    
  }

}
