import { Address } from "../address";

export interface Consultant{
    consultantId : number;
    firstName : string;
    lastName : string;
    email : string;
    phone : string;
    availabilityDate? : Date;
    gender : string;
    address : Address;
    photo? : Blob;
    title? : string;
    maritalStatus : string;
    linkedInUrl? : string;
}