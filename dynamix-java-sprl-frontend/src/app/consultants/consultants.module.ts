import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultantsComponent } from './consultants.component';
import { ConsultantsRoutingModule } from './consultants-routing.module';
import { DataViewModule } from 'primeng/dataview';
import { PanelModule } from 'primeng/panel';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { SplitButtonModule } from 'primeng/splitbutton';
import { TabViewModule } from 'primeng/tabview';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { AddConsultantComponent } from './add-consultant/add-consultant.component';
import { CalendarModule } from 'primeng/calendar';
import { MessageModule } from 'primeng/message';
import { FileUploadModule } from 'primeng/fileupload';
import { OverlayPanelModule } from 'primeng/overlaypanel';


@NgModule({
  declarations: [
    ConsultantsComponent,
    AddConsultantComponent
  ],
  imports: [
    CommonModule,
    ConsultantsRoutingModule,
    DataViewModule,
    PanelModule,
    DropdownModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    SplitButtonModule,
    TabViewModule,
    TableModule,
    ButtonModule,
    CalendarModule,
    MessageModule,
    FileUploadModule,
    OverlayPanelModule
  ]
})
export class ConsultantsModule { }
